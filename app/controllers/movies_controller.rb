class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index
    @all_ratings = Movie.all_ratings
    @movies = Movie.find(:all)
    @hilite_date =""
    @hilite_title =""
    if @selected.nil? 
      @selected = Hash.new
      @all_ratings.each do |key|
        @selected[key] = false
      end 
    end    
    
    if !params[:ratings].nil? 
      
      params[:ratings].each do |key,value|
        @selected[key] = true
      end
      @movies = Movie.where(:rating => params[:ratings].keys)
    end
  
   if params[:sort] == 'title'
     rating_filter =[]
     0.upto(params[:rating].size) do|i|
       if(params[:rating][i] == "true")
         rating_filter << @all_ratings[i]
         @selected[@all_ratings[i]] = true
       end
     end
     @movies = Movie.find(:all,:order =>'title ASC')  
     @movies = Movie.where(:rating => rating_filter).order('title ASC') unless rating_filter.empty?
     @hilite_title ='hilite'
  end

   if params[:sort] =='release_date'
     rating_filter =[]
     0.upto(params[:rating].size) do |i|
        if(params[:rating][i] =="true")
           rating_filter << @all_ratings[i]
           @selected[@all_ratings[i]] = true
        end
     end
    @movies = Movie.find(:all, :order => 'release_date ASC')
    @movies = Movie.where(:rating => rating_filter).order('release_date ASC') unless rating_filter.empty?
    @hilite_date ='hilite' 
  end

=begin
    @movies = Movie.where(:rating => params[:ratings].keys) unless params[:ratings].nil?
    
    if(params[:sort] =='title')
      @movies = Movie.find(:all,:order =>'title ASC')
      @movies = Movie.find(:all,:order =>'title ASC',:rating =>params[:ratings].keys) unless params[:rating].nil?
      @hilite_title = "hilite"
    end
    if(params[:sort] =='date')
      @movies = Movie.find(:all,:order =>'release_date ASC')
      @movies = Movie.find(:all,:order =>'release_date ASC',:rating =>params[:rating].keys) unless params[:rating].nil?
      @hilite_date ="hilite"
    end
=end 
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
