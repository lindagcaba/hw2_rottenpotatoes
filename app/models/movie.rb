class Movie < ActiveRecord::Base
  def self.all_ratings
     ratings = select(:rating).uniq
     all_ratings = Array.new
     ratings.each do |rating|
        all_ratings << rating[:rating]
     end
     all_ratings
  end  
end
